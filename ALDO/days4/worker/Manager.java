package days4.worker;

import java.util.ArrayList;
import java.util.Scanner;

public class Manager extends Worker {

    ArrayList<Integer> idKaryawan = new ArrayList<Integer>();
    ArrayList<String> namaKaryawan = new ArrayList<String>();
    ArrayList<Integer> salary = new ArrayList<Integer>();
    ArrayList<String> posisi = new ArrayList<String>();
    ArrayList<Integer> trans = new ArrayList<Integer>();
    ArrayList<Integer> ent = new ArrayList<Integer>();
    Scanner in = new Scanner(System.in); 

    public void tambahManager() {
        gajiPokok = 10000000;
        System.out.print("Masukkan Id : ");
        int idWorker = in.nextInt();

        System.out.print("Masukkan Nama : ");
        String nama = in.next();

        System.out.print("Jabatan : ");
        String jabatan = in.next();

        idKaryawan.add(idWorker);
        namaKaryawan.add(nama);
        posisi.add(jabatan);
        salary.add(gajiPokok);

        System.out.println("DATA BERHASIL DIMASUKKAN");
    }

    public void absenManager() {
        System.out.print("Masukkan Absen Dalam Sebulan : ");
        int hari = in.nextInt();
        absen = hari;
        System.out.print("Absen Hari dalam Sebulan : " + absen );
    }
    public void tunjanganTrans() {
        System.out.print("Masukkan Absen Hari dalam 1 Bulan : ");
        int hari = in.nextInt();
        absen = hari;
        int tunjanganTrans = 50000 * absen;
        trans.add(tunjanganTrans);
    }

    public void tunjanganEnt() {
        System.out.print("Masukkan Jumlah Entertaint : ");
        int entertaint = in.nextInt();
        absen = entertaint;
        int tunjanganEnt = 500000 * absen;
        ent.add(tunjanganEnt);
    }

}