package days4.socket;

import java.net.*;
import java.util.Scanner;
import java.io.*;
// import java.util.*;

public class GreetingClient {
    public static void main(String[] args) {
        boolean status = true;
        while (status) {

            String serverName = "Localhost";
            int port = 1111;
            try {
                System.out.println("Connecting to " + serverName + " on port " + port);
                Socket client = new Socket(serverName, port);

                System.out.println("Just connected to " + client.getRemoteSocketAddress());
                OutputStream outToServer = client.getOutputStream();
                DataOutputStream out = new DataOutputStream(outToServer);

                out.writeUTF("Hello from " + client.getLocalSocketAddress());
                InputStream inFromServer = client.getInputStream();
                DataInputStream in = new DataInputStream(inFromServer);

                System.out.println("Server says " + in.readUTF());

                Scanner input = new Scanner(System.in);
                System.out.println("Apakah Mau Close ? : y/n ");
                String tutup = input.next();

                if (tutup.equalsIgnoreCase("y")) {
                    status = false;
                    client.close();
                    break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}