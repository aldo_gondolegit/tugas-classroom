package days4.arraySiswa;

import java.util.ArrayList;
import java.util.Scanner;

public class Siswa {

    public void daftarMenu() {
        System.out.println("1. Tambah Data Siswa ");
        System.out.println("2. Edit Data Siswa");
        System.out.println("3. Hapus Data Siswa");
        System.out.println("4. Laporan Data Siswa");
        System.out.println("5. Exit");
    }

    public Siswa() {

        ArrayList<Integer> idSiswa = new ArrayList<Integer>();
        ArrayList<String> namaSiswa = new ArrayList<String>();
        ArrayList<Double> nilaiSiswa = new ArrayList<Double>();
        ArrayList<String> status = new ArrayList<String>();

        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("\t \n DAFTAR MENU \t");
            System.out.println("==============================");
            daftarMenu();
            System.out.print("Pilih Menu : ");

            int menu = in.nextInt();
            switch (menu) {
                case 1:
                    System.out.print("ID Siswa : ");
                    final int id = in.nextInt();
                    System.out.print("Nama Siswa : ");
                    String nama = in.next();
                    System.out.print("Nilai Siswa : ");
                    double nilai = in.nextDouble();
                    idSiswa.add(id);
                    namaSiswa.add(nama);
                    nilaiSiswa.add(nilai);
                    double nilaiStatus = nilai;
                    String grade;

                    if (nilaiStatus <= 20) {
                        grade = "E";
                        status.add(grade);
                    } else if (nilaiStatus >= 21 && nilaiStatus <= 40) {
                        grade = "D";
                        status.add(grade);
                    } else if (nilaiStatus >= 41 && nilaiStatus <= 60) {
                        grade = "C";
                        status.add(grade);
                    } else if (nilaiStatus >= 61 && nilaiStatus <= 80) {
                        grade = "B";
                        status.add(grade);
                    } else if (nilaiStatus >= 81 && nilaiStatus <= 100) {
                        grade = "A";
                        status.add(grade);
                    }

                    System.out.println("Data Berhasil Dimasukkan");

                    break;

                case 2:
                    System.out.print("Cari ID : ");
                    int cari = in.nextInt();
                    System.out.print("ID siswa : " + idSiswa.get(cari - 1));
                    System.out.print("\nNama : " + namaSiswa.get(cari - 1));
                    System.out.print("\nNilai Sebelum Diubah : " + nilaiSiswa.get(cari - 1));
                    System.out.print("\nMasukkan Nilai Baru : ");
                    double nilaiBaru = in.nextDouble();
                    nilaiSiswa.set(cari - 1, nilaiBaru);
                    System.out.print("\nNilai Setelah Diubah : " + nilaiSiswa.get(cari-1));
                    double nilaiStatus2 = nilaiBaru;
                    String grade2;

                    if (nilaiStatus2 <= 20) {
                        grade2 = "E";
                        status.set(cari-1,grade2);
                    } else if (nilaiStatus2 >= 21 && nilaiStatus2 <= 40) {
                        grade2 = "D";
                        status.set(cari-1,grade2);
                    } else if (nilaiStatus2 >= 41 && nilaiStatus2 <= 60) {
                        grade2 = "C";
                        status.set(cari-1,grade2);
                    } else if (nilaiStatus2 >= 61 && nilaiStatus2 <= 80) {
                        grade2 = "B";
                        status.set(cari-1,grade2);
                    } else if (nilaiStatus2 >= 81 && nilaiStatus2 <= 100) {
                        grade2 = "A";
                        status.set(cari-1,grade2);
                    }
                    break;

                case 3:
                    System.out.print("Hapus Berdasarkan ID : ");
                    int hapus = in.nextInt();
                        idSiswa.remove(hapus -1);
                        namaSiswa.remove(hapus -1);
                        nilaiSiswa.remove(hapus -1);
                    System.out.println("Data Berhasil Dihapus");
                    break;
                case 4:
                    System.out.println("+---------------+---------------+---------------+--------------+");
                    System.out.println("|       ID      |   NAMA SISWA  |     NILAI     |     GRADE    |");
                    System.out.println("+---------------+---------------+---------------+--------------+");
                    int tampil = idSiswa.size();
                    for (int i = 0; i < tampil; i++) {
                        System.out.print("| \t" + idSiswa.get(i) + "\t");

                        System.out.print("|\t" + namaSiswa.get(i) + "\t |");

                        System.out.print("\t" + nilaiSiswa.get(i) + "\t |");

                        System.out.print("\t" + status.get(i) + "\t|\n");

                    }
                    System.out.println("+---------------+---------------+---------------+--------------+");

                    break;
                case 5:
                    System.out.println("KELUAR");
                    System.exit(0);
                default:
                    break;
            }

        }
    }

    public static void main(String[] args) {
        Siswa run = new Siswa();
    }
}