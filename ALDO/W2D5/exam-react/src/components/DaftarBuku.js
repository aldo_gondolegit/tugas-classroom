import React, { Component } from 'react'

export default class DaftarBuku extends Component {
    render() {
        return (
            <tr className="table">
                <td>{this.props.buku.judul}</td>

                <td>{this.props.buku.terbit}</td>
            </tr>
        )
    }
}
