import React, { Component } from 'react';
import Penjaga from './Penjaga';
// import Penjaga from './Penjaga';
// import { 
//     BrowserRouter as Router, 
//     Route, 
//     Link, 
//     Switch 
// } from 'react-router-dom'; 

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''

        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log(e.target.value);
    }

    handleKirim = () => {
        if (this.state.username === "penjaga" && this.state.password === "penjaga") {
            console.log("INI NANTI KE PENJAGA");
        }
        else if (this.state.username === "member" && this.state.password === "member") {
            console.log("INI LOGIN MEMBER");

        }
    }

    render() {
        return (
            <>
                <div className="container">
                    <h1>LOGIN</h1>
                    <br />

                    <input type="text" className="form-control col-5 mb-3" name="username" placeholder="Username" value={this.state.username} onChange={this.handleChange} />
                    <input type="password" className="form-control col-5 mb-3" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} />

                    <button className="btn btn-primary" type="submit" value="Submit" onClick={this.handleKirim}>Kirim</button>
                </div>

                <Penjaga/>
            </>
        );
    }
}
