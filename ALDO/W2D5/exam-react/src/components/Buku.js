import React, { Component } from 'react'
import DaftarBuku from './DaftarBuku'
export default class Buku extends Component {
    constructor() {
        super();
        this.state = {
            buku: [{
                judul: 'Laskar Pelangi',
                terbit: 'Bentang Pusaka'

            }, {
                judul: 'Cinta Brontosaurus',
                terbit: 'Gagas Media'
            }]

        }
    }
    render() {
        return (
            <>
                <table>
                    <tbody>
                        {this.state.buku.map((judulKu, i) => <DaftarBuku key={i}
                            buku={judulKu} />)
                        }
                    </tbody>
                </table>
            </>
        )
    }
}
