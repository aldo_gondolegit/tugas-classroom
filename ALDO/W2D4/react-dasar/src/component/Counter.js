import React, { Component } from 'react'

class Counter extends Component {
    constructor() {
        super();
        this.state = {
            counter: 0
        }
        this.setNewNumber = this.setNewNumber.bind(this)
        this.setNewNumberDec = this.setNewNumberDec.bind(this)
    }
    setNewNumber() {
        this.setState({ counter: this.state.counter + 1 })
    }

    setNewNumberDec() {
        this.setState({ counter: this.state.counter - 1 })
    }
    render() {
        return (
            <div>

                <button onClick={this.setNewNumber}>INCREMENT</button>
                <br />

                <h1> {this.state.counter}</h1>

                <button onClick={this.setNewNumberDec}>DECREMENT</button>
            </div>
        )
    }
}
export default Counter;
