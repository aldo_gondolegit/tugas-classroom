import React, { Component } from 'react'

class DaftarNama extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.absen.id}</td>
                <td>{this.props.absen.name}</td>
                <td>{this.props.absen.age}</td>
            </tr>

        );
    }
}
export default DaftarNama;