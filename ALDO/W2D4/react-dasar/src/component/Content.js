import React, { Component } from 'react'

class Content extends Component {
    render() {
        return (
            <div>
                <button onClick={this.props.updateStateProp}>CLICK MEH</button>
                <h1>{this.props.myDataProp}</h1>

            </div>
        )
    }
}
export default Content;