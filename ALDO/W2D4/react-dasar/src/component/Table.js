import React, { Component } from 'react'
import Header from './Header';
import DaftarNama from './DaftarNama';

export default class Table extends Component {
    constructor() {
        super(); 
        this.state = {
            absen: [
                {
                    "id": 1,
                    "name": "Liana",
                    "age": "17"
                },
                {
                    "id": 2,
                    "name": "Aldo",
                    "age": "16"
                },
                {
                    "id": 3,
                    "name": "Fadlan",
                    "age": "18"
                },
                {
                    "id": 4,
                    "name": "Gilas",
                    "age": "18"
                },
                {
                    "id": 5,
                    "name": "Fenny",
                    "age": "25"
                }

            ],
        }
    }
    render() {
        return (
            <div>
                <Header />
                <table>
                    <tbody>
                        {this.state.absen.map((person, i) => <DaftarNama key={i}
                            absen={person} />)
                        }
                        {/* {this.state.data.map((person, i) => <TableRow key={i}
              data={person} />)} */}
                    </tbody>
                </table>
            </div>
        )
    }
}
