import React, { Component } from 'react';
// import TableRow from './component/TableRow.js';
import Content from './component/Content';
import Table from './component/Table'
import Counter from './component/Counter'
import { Link, Route, Switch, BrowserRouter, Router } from 'react-router-dom'

class App extends Component {
  constructor() {
    super();
    this.state = {

      counter: 0,
      nama: '',
      klik: 'INI DI KLIK SEBELUM DI UPDATE DI CHILD',
      alamat: 'JALANYUK'
      // data: [
      //   {
      //     "id": 1,
      //     "name": "Foo",
      //     "age": "20"
      //   },
      //   {
      //     "id": 2,
      //     "name": "Ter",
      //     "age": "30"
      //   },
      //   {
      //     "id": 3,
      //     "name": "Ku",
      //     "age": "40"
      //   }

      // ]
    }

    this.setNamaUpdate = this.setNamaUpdate.bind(this)
    this.updateState = this.updateState.bind(this)
  }



  setNamaUpdate(e) {
    this.setState({ nama: e.target.value })
    console.log(e.target.value);
  }

  updateState() {
    this.setState({ klik: 'Data Setelah Di Update' })
    //kita juga bisa memberi nilai langsung kedalam state
  }

  setAlamat = () => {
    this.setState({ alamat: "Saya di Serpong" });
  }

  render() {
    return (
      <>

        <br />
        <br />
        <input type="text" value={this.state.nama} onChange={this.setNamaUpdate}></input>
        <br />
        <br />
        {/* <button onClick={this.updateState}>Klik Meh</button> ini contoh untuk mengganti langsung di parent */}
        {/* <h1>{this.state.klik}</h1> */}

        <Content myDataProp={this.state.klik}
          updateStateProp={this.updateState}  ></Content>
        <br />
        <br />
        <button onClick={this.setAlamat}>Kirim</button>
        {/* <button onClick={() => {this.setAlamat()}}>Kirim</button> MEMBUAT FUNCTION DIDALAM BUTTON */}

        <br />
        <br />
        <h1> {this.state.alamat}</h1>
        {/* <Router> */}

          <Link to="/table">Table</Link>
          <br />
          <Link to="/counter">Counter</Link>
        {/* </Router> */}
        <Switch>
          {/* <Route path="/"></Route> */}
          <Route path="/table" component={Table}></Route>
          <Route path="/counter" component={Counter}></Route>
          {/* <Route path="/table" exact component={Table}></Route> */}
        </Switch>
      </>
    );
  }
}

export default App;