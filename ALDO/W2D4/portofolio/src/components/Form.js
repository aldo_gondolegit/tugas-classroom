import React, { Component } from 'react';
import Portofolio from "./Portofolio.js";
// import Aldo from "./aldo.jpg";
// <img src={Aldo} alt="Fotoku"/>
class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {

            nama: '',
            ttl: '',
            jk: '',
            alamat: '',
            agama: '',
            kwg: '',
            pendidikan: '',
            noTelp: 0,
            keahlian: ''
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
        console.log(e.target.value);

    }


    handleKirim = (event) => {
        alert(this.state.nama + this.state.agama + this.state.jk + this.state.ttl + this.state.alamat + this.state.kwg + this.state.pendidikan + this.state.noTelp + this.state.keahlian)
        event.preventDefault();

    }
    render() {
        return (
            <>
                <div className="container">
                    <form className="form-group" onSubmit={this.handleKirim}>

                        <div className="row mb-4">
                            <input type="text" className="form-control col-5" name="nama" placeholder="Nama" value={this.state.nama} onChange={this.handleChange} />
                            <input type="text" className="form-control col-5 offset-1" name="ttl" placeholder="Tempat, Tanggal Lahir" value={this.state.ttl} onChange={this.handleChange} />
                        </div>
                        <div className="row mb-4">
                            <input type="text" className="form-control col-5" name="alamat" placeholder="Alamat" value={this.state.alamat} onChange={this.handleChange} />
                            <input type="text" className="form-control col-5 offset-1" name="jk" placeholder="Jenis Kelamin" value={this.state.jk} onChange={this.handleChange} />
                        </div>
                        <div className="row mb-4">
                            <input type="text" className="form-control col-5" name="agama" placeholder="Agama" value={this.state.agama} onChange={this.handleChange} />
                            <input type="text" className="form-control col-5 offset-1" name="kwg" placeholder="Kewarganegaraan" value={this.state.kwg} onChange={this.handleChange} />
                        </div>
                        <div className="row mb-4">
                            <input type="text" className="form-control col-5" name="pendidikan" placeholder="Pendidikan" value={this.state.pendidikan} onChange={this.handleChange} />
                            <input type="number" className="form-control col-5 offset-1" name="noTelp" placeholder="Nomor Telepon" value={this.state.noTelp} onChange={this.handleChange} />
                        </div>

                        <div className="row mb-4">

                            <Portofolio />
                            <input type="text" className="form-control col-5" name="keahlian" placeholder="Keahlian" value={this.state.keahlian} onChange={this.handleChange} />

                            <button className="btn btn-primary offset-1" type="submit" value="Submit">Kirim</button>
                        </div>
                    </form>
                </div>

            </>
        )
    }
}
export default Form;
