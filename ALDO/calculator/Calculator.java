package calculator;

import java.util.Scanner;

public class Calculator {

    double num1;
    double num2;
    String operasi;

    public Calculator() {
        while (true) {

            Scanner in = new Scanner(System.in);
            Scanner in2 = new Scanner(System.in);
            Scanner op = new Scanner(System.in);

            System.out.println("Masukkan Angka Pertama : ");
            num1 = in.nextDouble();

            System.out.println("Masukkan Angka Kedua : ");
            num2 = in2.nextDouble();
            System.out.println("Operasi : ");
            operasi = op.nextLine();

            if (operasi.equalsIgnoreCase("tambah")) {
                tambah();
            } else if (operasi.equalsIgnoreCase("kurang")) {
                kurang();
            } else if (operasi.equalsIgnoreCase("kali")) {
                kali();
            } else if (operasi.equalsIgnoreCase("bagi")) {
                bagi();
            } else if (operasi.equalsIgnoreCase("mod")) {
                modulus();
            }
        }
    }

    public void tambah() {
        double tambah = num1 + num2;
        System.out.println("Hasil Penjumlahan : " + tambah);
    }

    public void kurang() {
        double kurang = num1 - num2;
        System.out.println("Hasil Pengkurangan : " + kurang);
    }

    public void kali() {
        double kali = num1 * num2;
        System.out.println("Hasil Perkalian : " + kali);
    }

    public void bagi() {
        double bagi = num1 / num2;
        System.out.println("Hasil Pembagian : " + bagi);
    }

    public void modulus() {
        double mod = num1 % num2;
        System.out.println("Hasil Pembagian : " + mod);
    }

}