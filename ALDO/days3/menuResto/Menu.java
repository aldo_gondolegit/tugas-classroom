package days3.menuResto;

import java.util.Scanner;

public class Menu {

    int pil;

    String menu1 = "Nasi Goreng Informatika \t Rp. 5.000,- \n";
    String menu2 = "Nasi Soto Ayam Internet \t Rp. 7.000,- \n";
    String menu3 = "Gado - Gado Disket \t\t Rp. 4.500,- \n";
    String menu4 = "Bubur Ayam LAN \t \t Rp. 4.000,- \n";

    public void menu() {
        System.out.println("\t MENU RESTORAN Mc'Cihuy \n");
        System.out.println("=================================== \n");
        System.out.println("1. " + menu1);
        System.out.println("2. " + menu2);
        System.out.println("3. " + menu3);
        System.out.println("4. " + menu4);
        System.out.println("=================================== \n");
    }

    public void pilih() {
        while (true) {

            Scanner in = new Scanner(System.in);
            System.out.print("Masukkan Pilihan Anda : ");
            pil = in.nextInt();
            
            switch (pil) {
                case 1:
                System.out.println("Pilihan  No. " + pil + " " + menu1);
                    break;

                case 2:
                System.out.println("Pilihan  No. " + pil + " " + menu2);
                    break;
                
                case 3:
                System.out.println("Pilihan  No. " + pil + " " + menu3);
                    break;

                case 4:
                System.out.println("Pilihan  No. " + pil + " " + menu4);
                    break;
            
                default:
                System.out.println("MENU PILIHAN TIDAK ADA! SILAHKAN COBA LAGI ");
                    break;
            }

            //Contoh Pake IF ELSE
            
            // if (pil == 1) {

            //     System.out.println("Pilihan  No. " + pil + " " + menu1);
            // } else if (pil == 2) {
            //     System.out.println("Pilihan  No. " + pil + " " + menu2);

            // } else if (pil == 3) {
                // System.out.println("Pilihan  No. " + pil + " " + menu3);

            // } else if (pil == 4) {
                // System.out.println("Pilihan  No. " + pil + " " + menu4);

            // }else{
                // System.out.println("MENU PILIHAN TIDAK ADA! SILAHKAN COBA LAGI ");
            // }
        }
    }

}