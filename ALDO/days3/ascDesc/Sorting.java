package days3.ascDesc;
import java.util.Collections;
import java.util.Arrays;

public class Sorting {

    public static void main(String[] args) {
        Integer[] angka = { 5, 1, 3, 2, 4 };

        System.out.println("\nData Asli : ");
        for (int i = 0; i < angka.length; i++) {
            System.out.print(angka[i] + ", ");
        }

        Arrays.sort(angka);
        System.out.println("\n\nPengurutan Ascending : ");
        for (int i : angka) {
            System.out.print(i + ", ");
        }
        
        
        System.out.println("\n\nPengurutan Descending : ");
        Arrays.sort(angka, Collections.reverseOrder());
        for (int i : angka) {
            System.out.print(i + ", ");
        }
    }
}