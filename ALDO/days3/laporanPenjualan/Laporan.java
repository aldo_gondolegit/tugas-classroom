package days3.laporanPenjualan;

import java.util.Scanner;

public class Laporan {

    public void input() {

        int[] jumlahBarang = new int[5];
        int[] harga = new int[5];
        String[] namaBarang = new String[5];

        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Bulan : ");
        int bulan = in.nextInt();
        System.out.print("Masukkan Jumlah Data : ");
        int jumlahData = in.nextInt();

        for (int i = 1; i <= jumlahData; i++) {
            System.out.print("\nNama Barang ke - " + i + " : ");
            String nama = in.next();
            namaBarang[i] = nama;
            System.out.print("Jumlah : ");
            int jumlah = in.nextInt();
            jumlahBarang[i] = jumlah;
            System.out.print("Harga : ");
            int hargaBarang = in.nextInt();
            harga[i] = hargaBarang;

        }
        System.out.println("LAPORAN PENJUALAN PT. NexSOFT Indonesia");

        System.out.println("BULAN : " + bulan);
        System.out.println("+----------------+---------------+---------------+-------------+-------------+");
        System.out.println("|       NO       |  NAMA BARANG  |    JUMLAH     |    HARGA    |    TOTAL    |");
        System.out.println("+----------------+---------------+---------------+-------------+-------------+");
        int totalPenjualan = 0;
        for (int i = 1; i <= jumlahData; i++) {
            System.out.println("|\t" + i + "\t " + "|\t " + namaBarang[i] + "\t" + "|\t" + jumlahBarang[i] + "\t"
                    + "|\t" + harga[i] + "\t" + "|\t" + (jumlahBarang[i] * harga[i]) + "|");

            totalPenjualan = totalPenjualan + (jumlahBarang[i] * harga[i]);
        }
        System.out.println("+----------------+---------------+---------------+-------------+-------------+");

        System.out.println("TOTAL BARANG : " + jumlahData);

        System.out.println("TOTAL PENJUALAN : " + totalPenjualan);
    }

}