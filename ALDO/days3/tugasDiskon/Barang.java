package days3.tugasDiskon;

import java.util.*;

public class Barang {

    public void beli() {
        while (true) {

            int harga;
            Scanner in = new Scanner(System.in);
            System.out.print("\nHarga Beli : ");
            harga = in.nextInt();

            System.out.println("Harga Awal : Rp. " + harga);

            int diskon = harga / 100 * 10;
            System.out.print("Diskon : Rp. " + diskon + "\n");

            int hargaFinal = harga - diskon;
            System.out.println("Harga Yang Harus Di Bayar = Rp. " + hargaFinal);
        }

    }
}