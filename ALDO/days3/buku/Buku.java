package days3.buku;

public class Buku {

    String jenisBuku;
    int id, jumlahPinjam, pilihBuku;
    String[] judulKomik = { "", "Naruto", "One Piece", "Dragon Ball", "Tsubasa", "Conan" };
    String[] pengarangKomik = { "", "Masashi Kishimoto", "Eiichiro Oda", "Akira Toriyama", "Yoichi Takahashi",
            "Aoyama Gosho" };
    String[] judulNovel = { "", "Laskar Pelangi", "Sang Pemimpi", "Hujan Bulan Juni" };
    String[] pengarangNovel = { "", "Andrea Hirata", "Andrea Hirata", "Sapardi Djoko Darmono" };

    public void pilihKomik(int pilihBuku) {
        String namaKomik = judulKomik[pilihBuku];
        String pengarang = pengarangKomik[pilihBuku];
    }

    public void pilihNovel(int pilihBuku) {

        String namaNovel = judulNovel[pilihBuku];
        String pengarang = pengarangNovel[pilihBuku];
    }

}
