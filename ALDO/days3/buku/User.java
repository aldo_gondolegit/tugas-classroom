package days3.buku;

import java.util.Scanner;

public class User extends Buku {

    String nama, alamat, email, jenisUser;
    float noHp;

    public void user(String jeniUserNya) {
        this.jenisUser = jeniUserNya;
        if (jeniUserNya.equalsIgnoreCase("member")) {
            Scanner in = new Scanner(System.in);
            System.out.println("\n Member Hanya Boleh Meminjam 5 Komik / 3 Novel / 2 Komik + 2 Novel \n");
            System.out.println("=====================================================================\n");
            System.out.print("Masukkan Jumlah Buku yang Ingin Di Pinjam : ");
            jumlahPinjam = in.nextInt();
            System.out.print("Masukkan Jenis Buku = ");
            jenisBuku = in.next();
    
            if (jenisBuku.equalsIgnoreCase("komik")) {
                Komik myKomik = new Komik();
                myKomik.judulKomik();
                for (int i = 0; i < jumlahPinjam; i++) {
                    System.out.print("Pilih Komik : ");
                    pilihBuku = in.nextInt();
                    pilihKomik(pilihBuku);
                    
                }
            } else if (jenisBuku.equalsIgnoreCase("novel")) {
                Novel myNovel = new Novel();
                myNovel.judulNovel();
                for (int i = 0; i < jumlahPinjam; i++) {
                    System.out.print("Pilih Novel : ");
                    pilihBuku = in.nextInt();
                    pilihNovel(pilihBuku);
                }
            }

        } else if (jeniUserNya.equalsIgnoreCase("umum")) {
            Scanner in = new Scanner(System.in);
            System.out.println("\n Umum Hanya Boleh Meminjam 3 Komik / 2 Novel / 1 Komik + 1 Novel \n");
            System.out.println("=====================================================================\n");
            System.out.print("Masukkan Jumlah Buku yang Ingin Di Pinjam : ");
            jumlahPinjam = in.nextInt();
            System.out.print("Masukkan Jenis Buku = ");
            jenisBuku = in.next();
            if (jenisBuku.equalsIgnoreCase("komik")) {
                Komik myKomik = new Komik();
                myKomik.judulKomik();
                for (int i = 0; i < jumlahPinjam; i++) {
                    System.out.print("Pilih Komik : ");
                    pilihBuku = in.nextInt();
                    pilihKomik(pilihBuku);
                }
            } else if (jenisBuku.equalsIgnoreCase("novel")) {
                Novel myNovel = new Novel();
                myNovel.judulNovel();
                for (int i = 0; i < jumlahPinjam; i++) {
                    System.out.print("Pilih Novel : ");
                    pilihBuku = in.nextInt();
                    pilihNovel(pilihBuku);
                }

            }
            
        }

    }

    public void peminjam() {

        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Jenis User : ");
        user(jenisUser = in.next());
        System.out.print("Nama : ");
        nama = in.next();
        System.out.print("Alamat : ");
        alamat = in.next();
        System.out.print("Email : ");
        email = in.next();
    }

    public static void main(String[] args) {
        User run = new User();
        // run.user(jeniUserNya);
        run.peminjam();
    }
}