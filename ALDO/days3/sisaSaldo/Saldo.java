package days3.sisaSaldo;

import java.util.Scanner;

public class Saldo {
    int saldo;
    double saldoAkhir;
    double bunga = 0.02;

    public void saldoKu() {
        Scanner in = new Scanner(System.in);

        System.out.print("\nMasukkan Saldo Anda : Rp. ");
        saldo = in.nextInt();

        System.out.println("Saldo Awal Rp. " + saldo);

        saldoAkhir = (bunga * saldo) + saldo; 

        System.out.println("Setelah berjalan satu Bulan dengan Pertimbangan bunga dan biaya Admin, Saldo Baru Anda adalah Rp. " + saldoAkhir);
    }

}