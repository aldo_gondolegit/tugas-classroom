package days3.musim;

import java.util.Scanner;

public class Musim {

    String bulan;

    public void musiman() {
        while (true) {

            Scanner in = new Scanner(System.in);
            System.out.print("Masukkan Bulan : ");
            bulan = in.nextLine();

            if (bulan.equalsIgnoreCase("desember") || bulan.equalsIgnoreCase("januari")
                    || bulan.equalsIgnoreCase("februari")) {
                System.out.println(bulan + " adalah Musim Dingin");
            } else if (bulan.equalsIgnoreCase("maret") || bulan.equalsIgnoreCase("april")
                    || bulan.equalsIgnoreCase("mei")) {
                System.out.println(bulan + " adalah Musim Semi");
            } else if (bulan.equalsIgnoreCase("juni") || bulan.equalsIgnoreCase("juli")
                    || bulan.equalsIgnoreCase("agustus")) {
                System.out.println(bulan + " adalah Musim Panas");
            } else if (bulan.equalsIgnoreCase("september") || bulan.equalsIgnoreCase("oktober")
                    || bulan.equalsIgnoreCase("november")) {
                System.out.println(bulan + " adalah Musim Gugur");
            }
        }
    }
}