package days3.overloading;

public class App {
    public static void main(String[] args) {
        Overloading run = new Overloading();
        int a = 11;
        int b = 6;
        double c = 7.3;
        double d = 9.4;
        int result1 = run.minFunction(a, b);

        double result2 = run.minFunction(c, d);
        System.out.println("Minimum Value = " + result1);
        System.out.println("Minimum Value = " + result2);
    }

}