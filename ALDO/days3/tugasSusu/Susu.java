package days3.tugasSusu;

import java.util.Scanner;

public class Susu {
    int kodeSusu, jumlahBeli, totalHarga;
    String ukuran;

    int stokBesar = 100;
    int stokSedang = 90;
    int stokKecil = 80;
    int stokAkhir;

    String[] jenisSusu = { "Susu Nasional", "Susu Indomilk", "Susu Tiga Sapi" };
    int[] hargaBesar = { 8000, 10000, 7000 };
    int[] hargaSedang = { 6000, 6500, 7000 };
    int[] hargaKecil = { 3000, 4000, 5000 };

    public void beli() {
        while (true) {

            Scanner in = new Scanner(System.in);
            Scanner in2 = new Scanner(System.in);
            Scanner in3 = new Scanner(System.in);

            System.out.print("Masukkan Kode Susu (1 / 3) : ");
            kodeSusu = in.nextInt();
            System.out.print("Masukkan Jumlah Beli : ");
            jumlahBeli = in2.nextInt();
            System.out.println("Masukkan Ukuran (B / S / K) : ");
            ukuran = in3.nextLine();

            switch (kodeSusu) {
                case 1:
                    System.out.println("Jenis Susu : " + jenisSusu[0]);

                    break;
                case 2:
                    System.out.println("Jenis Susu : " + jenisSusu[1]);

                    break;
                case 3:
                    System.out.println("Jenis Susu : " + jenisSusu[2]);
                    break;

                default:
                    System.out.println("Kode Susu Tidak Terdaftar");
                    break;
            }

            if (jumlahBeli > stokBesar || jumlahBeli > stokSedang || jumlahBeli > stokKecil) {
                System.out.println("STOK TIDAK MEMENUHI DARI PERMINTAAN");
            }
            //======================PRODUK 1 ======================
            if (kodeSusu == 1 && ukuran.equals("B") && jumlahBeli <= stokBesar) {
                totalHarga = jumlahBeli * hargaBesar[0];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokBesar - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaBesar[0]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            } 
            
            else if(kodeSusu == 1 && ukuran.equals("S") && jumlahBeli <= stokSedang){
                totalHarga = jumlahBeli * hargaSedang[0];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokSedang - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaSedang[0]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            
            else if(kodeSusu == 1 && ukuran.equals("K") && jumlahBeli <= stokKecil){
                totalHarga = jumlahBeli * hargaKecil[0];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokKecil - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaKecil[0]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            //========================================================


            //======================PRODUK 2 ======================
            else if(kodeSusu == 2 && ukuran.equals("B") && jumlahBeli <= stokBesar ){
                totalHarga = jumlahBeli * hargaBesar[1];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokBesar - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaBesar[1]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            
            else if(kodeSusu == 2 && ukuran.equals("S") && jumlahBeli <= stokSedang){
                totalHarga = jumlahBeli * hargaSedang[1];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokSedang - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaSedang[1]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            
            else if(kodeSusu == 2 && ukuran.equals("K") && jumlahBeli <= stokKecil){
                totalHarga = jumlahBeli * hargaKecil[1];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokKecil - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaKecil[1]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            //========================================================
            

            //======================PRODUK 3 =======================
            else if(kodeSusu == 3 && ukuran.equals("B")&& jumlahBeli <= stokBesar){
                totalHarga = jumlahBeli * hargaBesar[2];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokBesar - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaBesar[2]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            
            else if(kodeSusu == 3 && ukuran.equals("S")&& jumlahBeli <= stokSedang){
                totalHarga = jumlahBeli * hargaSedang[2];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokSedang - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaSedang[2]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            
            else if(kodeSusu == 3 && ukuran.equals("K") && jumlahBeli <= stokKecil){
                totalHarga = jumlahBeli * hargaKecil[2];
                System.out.println("Stok Sebelum DiBeli : " + stokBesar);
                stokAkhir = stokKecil - jumlahBeli;
                System.out.println("Stok Setelah Dibeli : " + stokAkhir);
                System.out.println("Harga : " + hargaBesar[2]);
                System.out.println("TOTAL PEMBAYARAN :" + totalHarga);
            }
            //========================================================

        
        }

    }
}

// switch (kodeSusu) {
// case 1:
// System.out.println("Jenis Susu : " + jenisSusu[0]);
// // ukuran = in.nextLine();
// if (ukuran.equalsIgnoreCase("b")) {
// System.out.println("Harga : Rp. " + hargaBesar[0]);

// totalHarga = jumlahBeli * hargaBesar[0];
// System.out.println("Total Harga Pembelian : " + totalHarga);
// } else if (ukuran.equalsIgnoreCase("s")) {
// System.out.println("Harga : Rp. " + hargaSedang[0]);

// totalHarga = jumlahBeli * hargaSedang[0];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// } else if (ukuran.equalsIgnoreCase("k")) {
// System.out.println("Harga : Rp. " + hargaKecil[0]);

// totalHarga = jumlahBeli * hargaKecil[0];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// }

// break;
// case 2:
// System.out.println("Jenis Susu : " + jenisSusu[1]);
// if (ukuran.equalsIgnoreCase("b")) {
// System.out.println("Harga : Rp. " + hargaBesar[1]);

// totalHarga = jumlahBeli * hargaBesar[1];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// } else if (ukuran.equalsIgnoreCase("s")) {
// System.out.println("Harga : Rp. " + hargaSedang[1]);

// totalHarga = jumlahBeli * hargaSedang[1];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// } else if (ukuran.equalsIgnoreCase("k")) {
// System.out.println("Harga : Rp. " + hargaKecil[1]);

// totalHarga = jumlahBeli * hargaKecil[1];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// }
// break;
// case 3:
// System.out.println("Jenis Susu : " + jenisSusu[2]);
// if (ukuran.equalsIgnoreCase("b")) {
// System.out.println("Harga : Rp. " + hargaBesar[2]);

// totalHarga = jumlahBeli * hargaBesar[2];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// } else if (ukuran.equalsIgnoreCase("s")) {
// System.out.println("Harga : Rp. " + hargaSedang[2]);

// totalHarga = jumlahBeli * hargaSedang[2];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// } else if (ukuran.equalsIgnoreCase("k")) {
// System.out.println("Harga : Rp. " + hargaKecil[2]);

// totalHarga = jumlahBeli * hargaKecil[2];
// System.out.println("Total Harga Pembelian : " + totalHarga);

// }
// break;

// default:
// System.out.println("Kode Susu Tidak Terdaftar");
// break;
// }

// switch (ukuran) {
// case "B":
// System.out.println("Harga : Rp. " + hargaBesar[0]);

// totalHarga = jumlahBeli * hargaBesar[0];
// break;
// case "S":

// break;
// case "K":

// break;

// default:
// break;
// }