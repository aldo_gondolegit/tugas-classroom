package days3.tagihanListrik;

public class Tagihan {
    int pulsa = 110;
    float biaya = 20000;
    float tarif = 250;
    String noPelanggan = "001";
    String nama = "ALDO";
    String bulan = "Agustus";
    float jumlah = (pulsa * tarif) + biaya;

    public void tampil() {
        System.out.println("\nNomor Pelanggan : " + noPelanggan);
        System.out.println("\nNama Pelanggan : " + nama);
        System.out.println("\nBulan Tagihan : " + bulan);
        System.out.println("\nBanyak Pulsa Pemakaian : " + pulsa);
        System.out.println("\nJumlah Tagihan : " + jumlah);
    }

}