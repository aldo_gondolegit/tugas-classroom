package days2;

public class Car {
    int tahun = 2020;
    String merk = "Toyota Alphard";
    int kecepatan;

    public void tambahKecepatan(int kecepatan) {
        this.kecepatan = kecepatan;
    }

    public int getKecepatan() {
        System.out.println(merk);
        System.out.println(tahun);
        System.out.println("Kecepatan Setelah ditambah : " + kecepatan + " Km/jam");
        return kecepatan;
        
    }
    
    public void kurangKecepatan(int kecepatan) {
        this.kecepatan = kecepatan;
    }
    
    public int getKurangKecepatan() {
        System.out.println(merk);
        System.out.println(tahun);
        System.out.println("Kecepatan Setelah dikurangi : " + kecepatan+ " Km/jam");
        return kecepatan;
    }

    public static void main(String[] args) {
        Car run = new Car();
        System.out.println("\n");
        run.tambahKecepatan(150);
        run.getKecepatan();
        System.out.println("\n");
        run.kurangKecepatan(100);
        run.getKurangKecepatan();
    }
}