package days2;

public class App {
    public static void main(String[] args) {
        Tugas run = new Tugas();
        //PERSON SATU
        run.nama("Ardi");
        run.nilaiUTS(90);
        run.getUTS();
        run.nilaiUAS(70);
        run.getUAS();
        run.nilaiTugas(90);
        run.getTugas();
        run.nilaiAkhir();
        run.def();
        run.tampilkan();
        
        //PERSON 2
        run.nama("Pian");
        run.nilaiUTS(80);
        run.getUTS();
        run.nilaiUAS(70);
        run.getUAS();
        run.nilaiTugas(90);
        run.getTugas();
        run.nilaiAkhir();
        // run.def();
        run.tampilkan();

        //PERSON3
        run.nama("Robi");
        run.nilaiUTS(70);
        run.getUTS();
        run.nilaiUAS(60);
        run.getUAS();
        run.nilaiTugas(90);
        run.getTugas();
        run.nilaiAkhir();
        // run.def();
        run.tampilkan();
        run.foo();
    }
}