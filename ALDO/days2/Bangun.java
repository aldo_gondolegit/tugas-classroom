package days2;

public class Bangun {
    // Balok
    double p = 10;
    double l = 8;
    double t = 5;

    // Bola
    double phi = 3.14;
    double r = 10;

    // Kubus
    double rsk = 5;

    // Rumus
    double hasilBalok = p * l * t;
    double hasilBola = phi * r * r * r * 4 / 3;
    double hasilKubus = rsk * rsk * rsk;

    public void inisial() {
        System.out.println("Panjang Balok = " + p);
        System.out.println("Lebar Balok = " + l);
        System.out.println("Tinggi Balok = " + t);
        System.out.println("\n");
        System.out.println("Nilai PHI = " + phi);
        System.out.println("Jari-Jari Bola = " + r);
        System.out.println("\n");
        System.out.println("Rusuk Kubus = " + rsk);


    }

    public void balok() {
        System.out.println("Volume Balok Adalah : " + hasilBalok);
    }

    public void bola() {
        System.out.println("Volume Bola Adalah : " + Math.round(hasilBola));
    }

    public void kubus() {
        System.out.println("Volume Kubus Adalah : " + hasilKubus);
    }

    public void avg() {
        double avg = (hasilBalok + hasilBola + hasilKubus) / 3;
        System.out.println("AVERAGE dari ke Tiga ( 3 ) Volume Bangun Ruang tersebut adalah : " + Math.round(avg));
    }

    public void sum() {
        double sum = (hasilBalok + hasilBola + hasilKubus);
        System.out.println("SUMMARY dari ke Tiga ( 3 ) Volume Bangun Ruang tersebut adalah : " + Math.round(sum));

    }

    public static void main(String[] args) {
        Bangun run = new Bangun();
        System.out.println("\n");
        run.inisial();
        System.out.println("\n");
        run.balok();
        run.bola();
        run.kubus();
        run.avg();
        run.sum();
    }
}