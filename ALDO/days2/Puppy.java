package days2;

    public class Puppy {

        int puppyAge;
    
        public Puppy(String name){  //membuat constructor dengan parameter name
            System.out.println("My Name is = " + name);
        }
        
        public void setAge(int age) {
            puppyAge = age; //setter diambil dari class variable 
        }
    
        public int getAge() {
            System.out.println("Puppy Age is = " + puppyAge);
            return puppyAge;
        }
    
        public static void main(String[] args) {
            System.out.println("\n");
            Puppy myPuppy = new Puppy("ALDO"); //membuat objek dengan memberi nilai kedalam paramater
    
            //memanggil class method untuk memberi nilai pada puppyAge
            myPuppy.setAge(5);
    
            //Getter untuk mengambil Value dari umur puppy
            myPuppy.getAge();
    
            System.out.println("Variable Value : " + myPuppy.puppyAge);
        }
    }