package matrik;

public class Matrik {

    int[][] matrik1 = { { 7, 9, 5 }, { 1, 5, 0 }, { 4, 1, 2 } };

    int[][] matrik2 = { { 5, 7, 3 }, { 0, 4, 6 }, { 3, 4, 5 } };

    public void jumlah() {
        int _1 = matrik1[0][0] + matrik2[0][0];
        int _2 = matrik1[0][1] + matrik2[0][1];
        int _3 = matrik1[0][2] + matrik2[0][2];
        int _4 = matrik1[1][0] + matrik2[1][0];
        int _5 = matrik1[1][1] + matrik2[1][1];
        int _6 = matrik1[1][2] + matrik2[1][2];
        int _7 = matrik1[2][0] + matrik2[2][0];
        int _8 = matrik1[2][1] + matrik2[2][1];
        int _9 = matrik1[2][2] + matrik2[2][2];

        System.out.println("_______________________________________________\n");
        System.out.println("============ SETELAH DIJUMLAHKAN ==============");
        System.out.print("" + _1);
        System.out.print(" " + _2);
        System.out.print(" " + _3);
        System.out.println();
        System.out.print(" " + _4);
        System.out.print(" " + _5);
        System.out.print(" " + _6);
        System.out.println();
        System.out.print(" " + _7);
        System.out.print(" " + _8);
        System.out.print(" " + _9);
        System.out.println();
        System.out.println("_______________________________________________\n\n");

    }
    
    public void kurang() {
        int __1 = matrik1[0][0] - matrik2[0][0];
        int __2 = matrik1[0][1] - matrik2[0][1];
        int __3 = matrik1[0][2] - matrik2[0][2];
        int __4 = matrik1[1][0] - matrik2[1][0];
        int __5 = matrik1[1][1] - matrik2[1][1];
        int __6 = matrik1[1][2] - matrik2[1][2];
        int __7 = matrik1[2][0] - matrik2[2][0];
        int __8 = matrik1[2][1] - matrik2[2][1];
        int __9 = matrik1[2][2] - matrik2[2][2];

        System.out.println("_______________________________________________\n");
        System.out.println("============ SETELAH DIKURANGI ==============");
        System.out.print("" + __1);
        System.out.print(" " + __2);
        System.out.print(" " + __3);
        System.out.println();
        System.out.print("" + __4);
        System.out.print(" " + __5);
        System.out.print("" + __6);
        System.out.println();
        System.out.print("" + __7);
        System.out.print(" " + __8);
        System.out.print("" + __9);
        System.out.println();
        System.out.println("_______________________________________________\n\n");
    }
}